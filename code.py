import schedule

from crontab import CronTab


def cron_tab():
    my_cron = CronTab(user=True)

    job = my_cron.new(command='echo "Hello world"')

    job.minute.on(1)

    new_job = my_cron.new(command='echo "world Hello"')

    new_job.minute.on(1)

    my_cron.write()


def write_code():
    print(f'Хорошо написанная программа - это программа, написанная 2 раза')


if __name__ == "__main__":

    cron_tab()

    schedule.every().day.at("23:15").do(write_code)

    while True:
        schedule.run_pending()
